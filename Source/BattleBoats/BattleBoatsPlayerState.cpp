// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "BattleBoatsPlayerState.h"

void ABattleBoatsPlayerState::CopyProperties(class APlayerState* PlayerState)
{
	APlayerState::CopyProperties(PlayerState);

	if (PlayerState != nullptr)
	{
		ABattleBoatsPlayerState* battleBoatsPlayerState = Cast<ABattleBoatsPlayerState>(PlayerState);
		if (battleBoatsPlayerState)
		{
			battleBoatsPlayerState->IsReady		= IsReady;
			battleBoatsPlayerState->Team		= Team;
			battleBoatsPlayerState->TeamColor	= TeamColor;
		}
	}
}

void ABattleBoatsPlayerState::SeamlessTravelTo(class APlayerState* PlayerState)
{
	APlayerState::SeamlessTravelTo(PlayerState);

	if (PlayerState != nullptr)
	{
		ABattleBoatsPlayerState* battleBoatsPlayerState = Cast<ABattleBoatsPlayerState>(PlayerState);

		if (battleBoatsPlayerState)
		{
			battleBoatsPlayerState->IsReady = IsReady;
			battleBoatsPlayerState->Team = Team;
			battleBoatsPlayerState->TeamColor = TeamColor;
		}
	}
}

void ABattleBoatsPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABattleBoatsPlayerState, IsReady);
	DOREPLIFETIME(ABattleBoatsPlayerState, Team);
	DOREPLIFETIME(ABattleBoatsPlayerState, TeamColor);
}
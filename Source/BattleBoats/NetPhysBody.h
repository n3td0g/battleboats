// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SmoothPhysicsState.h"
#include "BaseBoatPlayerController.h"
#include "NetPhysBody.generated.h"

#define PROXY_STATE_ARRAY_SIZE 20

UCLASS()
class BATTLEBOATS_API ANetPhysBody : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANetPhysBody();
private:
	ABaseBoatPlayerController *playerController;
	UPrimitiveComponent *physComponent;

	FSmoothPhysicsState proxyStates[PROXY_STATE_ARRAY_SIZE];
	int proxyStateCount;

	void ClientSimulateMoving();
public:
	UPROPERTY(BlueprintReadWrite, Replicated)
	bool ReplicatePhysics;
	
	UPROPERTY(ReplicatedUsing = OnRep_ServerPhysicsState)
	FSmoothPhysicsState ServerPhysicsState;
	UFUNCTION()
	void OnRep_ServerPhysicsState();

	virtual void BeginPlay() override;
	void Tick(float DeltaSeconds) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "BattleBoatsGameState.h"

void ABattleBoatsGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABattleBoatsGameState, NumOfRevivals);
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "BattleBoatsGameMode.h"


void ABattleBoatsGameMode::KickPlayer(APlayerController * KickedPlayer, FText KickReason)
{
	if (GameSession && GameSession->IsValidLowLevel())
	{
		GameSession->KickPlayer(KickedPlayer, KickReason);
	}
}




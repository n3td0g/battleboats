// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "BaseBoatPlayerController.h"
#include <chrono>

using namespace std::chrono;

ABaseBoatPlayerController::ABaseBoatPlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	this->SetActorTickEnabled(true);
	PrimaryActorTick.bCanEverTick = true;
	timeServerTimeRequestWasPlaced = 0;
	timeOffsetFromServer = 0;
	timeOffsetIsValid = false;
}


/** Gets the current system time in milliseconds */
/* static */ int64 ABaseBoatPlayerController::GetLocalTime()
{
	milliseconds ms = duration_cast< milliseconds >(
		high_resolution_clock::now().time_since_epoch()
		);
	return (int64)ms.count();
}

/** True if the network time is valid. */
bool ABaseBoatPlayerController::IsNetworkTimeValid()
{
	return timeOffsetIsValid;
}

/** Gets the approximate current network time in milliseconds. */
int64 ABaseBoatPlayerController::GetNetworkTime()
{
	return GetLocalTime() + timeOffsetFromServer;
}

void ABaseBoatPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// Ask the server for its current time
	if (Role < ROLE_Authority)
	{
		timeServerTimeRequestWasPlaced = GetLocalTime();
		ServerGetServerTime();
	}
}

bool ABaseBoatPlayerController::ServerGetServerTime_Validate()
{
	return true;
}

/** Sent from a client to the server to get the server's system time */
void ABaseBoatPlayerController::ServerGetServerTime_Implementation()
{
	ClientGetServerTime(GetLocalTime());
}

/** Sent from the server to a client to give them the server's system time */
void ABaseBoatPlayerController::ClientGetServerTime_Implementation(int64 serverTime)
{
	int64 localTime = GetLocalTime();

	// Calculate the server's system time at the moment we actually sent the request for it.
	int64 roundTripTime = localTime - timeServerTimeRequestWasPlaced;
	serverTime -= roundTripTime / 2;

	// Now calculate the difference between the two values
	timeOffsetFromServer = serverTime - timeServerTimeRequestWasPlaced;

	// Now we can safely say that the following is true
	//
	// serverTime = timeServerTimeRequestWasPlaced + timeOffsetFromServer
	//
	// which is another way of saying
	//
	// NetworkTime = LocalTime + timeOffsetFromServer

	timeOffsetIsValid = true;
}




// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "UnrealNetwork.h"
#include "NetPhysBody.h"


ANetPhysBody::ANetPhysBody()
{
	proxyStateCount = 0;
	this->SetActorTickEnabled(true);
	PrimaryActorTick.bCanEverTick = true;
	ReplicatePhysics = true;	
	bReplicateMovement = false;
	bReplicates = true;
}

void ANetPhysBody::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ANetPhysBody, ServerPhysicsState);
	DOREPLIFETIME(ANetPhysBody, ReplicatePhysics);
}

void ANetPhysBody::OnRep_ServerPhysicsState()
{
	for (int i = PROXY_STATE_ARRAY_SIZE - 1; i >= 1; i--)
	{
		proxyStates[i] = proxyStates[i - 1];
	}
	proxyStates[0] = ServerPhysicsState;
	proxyStateCount = FMath::Min(proxyStateCount + 1, PROXY_STATE_ARRAY_SIZE);
	if (proxyStates[0].timestamp < proxyStates[1].timestamp)
	{
		UE_LOG(LogOnlineGame, Verbose, TEXT("Timestamp inconsistent: %d should be greater than %d"), proxyStates[0].timestamp, proxyStates[1].timestamp);
	}
}


void ANetPhysBody::ClientSimulateMoving()
{
	if (nullptr == playerController || !playerController->IsNetworkTimeValid() || 0 == proxyStateCount)
	{

	}
	else
	{
		uint64 interpolationBackTime = 100;
		uint64 extrapolationLimit = 500;

		uint64 interpolationTime = playerController->GetNetworkTime() - interpolationBackTime;

		if (proxyStates[0].timestamp > interpolationTime)
		{
			for (int i = 0; i<proxyStateCount; i++)
			{
				if (proxyStates[i].timestamp <= interpolationTime || i == proxyStateCount - 1)
				{
					FSmoothPhysicsState rhs = proxyStates[FMath::Max(i - 1, 0)];
					FSmoothPhysicsState lhs = proxyStates[i];


					int64 length = (int64)(rhs.timestamp - lhs.timestamp);
					double t = 0.0F;
					if (length > 1)
						t = (double)(interpolationTime - lhs.timestamp) / (double)length;

					FVector pos = FMath::Lerp(lhs.pos, rhs.pos, t);
					FVector vel = FMath::Lerp(lhs.vel, rhs.vel, t);
					SetActorLocationAndRotation(pos, GetActorRotation());
					physComponent->SetPhysicsLinearVelocity(vel);
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("MineLocation x: %f, y: %f"), pos.X, pos.Y));
					return;
				}
			}
		}
		else
		{
			FSmoothPhysicsState latest = proxyStates[0];
			uint64 extrapolationLength = interpolationTime - latest.timestamp;
			if (extrapolationLength < extrapolationLimit)
			{
				FVector pos = latest.pos + latest.vel * ((float)extrapolationLength * 0.001f);
				SetActorLocationAndRotation(pos, GetActorRotation());
				physComponent->SetPhysicsLinearVelocity(latest.vel);
			}
		}
	}
}


void ANetPhysBody::BeginPlay()
{
	Super::BeginPlay();

	physComponent = Cast<UPrimitiveComponent>(GetRootComponent());
	playerController = Cast<ABaseBoatPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	/*if (Role < ROLE_Authority)
	{
		physComponent->PutRigidBodyToSleep();
		physComponent->SetSimulatePhysics(false);
		physComponent->SetEnableGravity(false);
		SetActorEnableCollision(false);
	}*/
}

void ANetPhysBody::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	/*if (ReplicatePhysics)
	{
		if (Role < ROLE_Authority)
		{
			ClientSimulateMoving();
		}
		else
		{
			ServerPhysicsState.pos = GetActorLocation();
			ServerPhysicsState.rot = GetActorRotation();
			ServerPhysicsState.vel = physComponent->GetComponentVelocity();
			ServerPhysicsState.timestamp = ABaseBoatPlayerController::GetLocalTime();
		}
	}*/
}


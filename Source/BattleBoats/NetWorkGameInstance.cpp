// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "NetWorkGameInstance.h"

UNetWorkGameInstance::UNetWorkGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (GEngine != nullptr)
		settings = GEngine->GameUserSettings;
	else
		settings = nullptr;
}

EResolutionEnum UNetWorkGameInstance::GetResolution()
{
	if (!settings)
		return EResolutionEnum::VE_Res1280x720;

	FIntPoint res = settings->GetLastConfirmedScreenResolution();
	switch (res.X)
	{
	case 800:
		return EResolutionEnum::VE_Res800x600;
	case 1024:
		return EResolutionEnum::VE_Res1024x768;
	case 1600:
		return EResolutionEnum::VE_Res1600x900;
	case 1920:
		return EResolutionEnum::VE_Res1920x1080;
	case 1280:
	{
		if (res.Y == 1024)
			return EResolutionEnum::VE_Res1280x1024;
		return EResolutionEnum::VE_Res1280x720;
	}
	}

	return EResolutionEnum::VE_Res1280x720;
}

EQualityEnum UNetWorkGameInstance::GetQuality()
{
	if (!settings)
		return EQualityEnum::VE_High;
	switch (settings->ScalabilityQuality.GetSingleQualityLevel())
	{
	case 0:
		return EQualityEnum::VE_Low;
	case 1:
		return EQualityEnum::VE_Medium;
	case 2:
		return EQualityEnum::VE_High;
	case 3:
		return EQualityEnum::VE_Epic;
	}
	return EQualityEnum::VE_High;
}

EDisplayModeEnum UNetWorkGameInstance::GetDisplayMode()
{
	if (!settings)
		return EDisplayModeEnum::VE_Window;

	switch (settings->GetLastConfirmedFullscreenMode())
	{
	case EWindowMode::Type::Fullscreen:
		return EDisplayModeEnum::VE_Fullscreen;
	default:
		return EDisplayModeEnum::VE_Window;
	}
}

bool UNetWorkGameInstance::SaveSettings(EResolutionEnum resolution, EQualityEnum quality, EDisplayModeEnum displayMode)
{
	if (!settings)
		return false;

	int32 width = 1280, height = 720;

	switch (resolution)
	{
	case EResolutionEnum::VE_Res800x600:
	{
		width = 800;
		height = 600;
		break;
	}
	case EResolutionEnum::VE_Res1024x768:
	{
		width = 1024;
		height = 768;
		break;
	}
	case EResolutionEnum::VE_Res1280x720:
	{
		width = 1280;
		height = 720;
		break;
	}
	case EResolutionEnum::VE_Res1280x1024:
	{
		width = 1280;
		height = 1024;
		break;
	}
	case EResolutionEnum::VE_Res1600x900:
	{
		width = 1600;
		height = 900;
		break;
	}
	case EResolutionEnum::VE_Res1920x1080:
	{
		width = 1920;
		height = 1080;
		break;
	}
	}

	EWindowMode::Type winMode = displayMode == EDisplayModeEnum::VE_Window ? EWindowMode::Type::Windowed : EWindowMode::Type::Fullscreen;

	settings->RequestResolutionChange(width, height, winMode, false);
	settings->SetScreenResolution(FIntPoint(width, height));
	settings->SetFullscreenMode(winMode);
	if (displayMode == EDisplayModeEnum::VE_Window)
		settings->SetWindowPosition(0, 0);
	else
		settings->SetWindowPosition(-1, -1);
	settings->ConfirmVideoMode();

	int32 q = 3;
	switch (quality)
	{
	case EQualityEnum::VE_Low:
		q = 0;
		break;
	case EQualityEnum::VE_Medium:
		q = 1;
		break;
	case EQualityEnum::VE_High:
		q = 2;
		break;
	case EQualityEnum::VE_Epic:
		q = 3;
		break;
	}

	settings->ScalabilityQuality.SetFromSingleQualityLevel(q);
	settings->ApplyNonResolutionSettings();

	settings->SaveSettings();

	return true;
}
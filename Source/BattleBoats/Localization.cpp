// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "Localization.h"

void ULocalization::changeLocalization(FString target)
{
	FInternationalization::Get().SetCurrentCulture(target);
}

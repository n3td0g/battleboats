// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Localization.generated.h"

/**
 * 
 */
UCLASS()
class BATTLEBOATS_API ULocalization : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	/* Change Localization at Runtime. */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Change Localization"), Category = "Locale")
		static void changeLocalization(FString target);
	
	
	
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BattleBoatsAIController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLEBOATS_API ABattleBoatsAIController : public AAIController
{
	GENERATED_BODY()
	
	
public:
	ABattleBoatsAIController(const FObjectInitializer &ObjectInitializer);
	
};

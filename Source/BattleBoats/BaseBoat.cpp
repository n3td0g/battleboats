// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoats.h"
#include "UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"
#include "BaseBoatPlayerController.h"
#include "BaseBoat.h"


void ABaseBoat::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseBoat, ServerPhysicsState);
	DOREPLIFETIME(ABaseBoat, MovementScale);
	DOREPLIFETIME(ABaseBoat, RotationScale);
}

bool ABaseBoat::ServerApplyInput_Validate(FVector movementInput, FVector aimingInput, bool backMovement, bool isGamepadInput)
{
	return true;
}

void ABaseBoat::ServerApplyInput_Implementation(FVector movementInput, FVector aimingInput, bool backMovement, bool isGamepadInput)
{
	ApplyInput(movementInput, aimingInput, backMovement, isGamepadInput);
}

void ABaseBoat::RotateKit(FVector direction)
{
	if (direction.Size() > 0.0f)
	{
		float r_kit = FRotator::NormalizeAxis(UKismetMathLibrary::MakeRotFromX(direction).Yaw - GetActorRotation().Yaw);
		float r_old = FRotator::NormalizeAxis(KitAngle); //	
		KitAngle = FMath::Lerp(r_old, r_kit, 0.5f);// FMath::FInterpTo(r_kit, r_old, GetWorld()->GetDeltaSeconds(), 1.0f);
		KitAngle = FMath::Clamp(KitAngle, -135.0f, 135.0f);
	}	
}

void ABaseBoat::ApplyInput(FVector movementInput, FVector aimingInput, bool backMovement, bool isGamepadInput)
{
	//���� ��������� ����� ��� �� �� ������� - ��������� ��������
	if (IsLocallyControlled() || Role == ROLE_Authority)
	{
		UPrimitiveComponent *root = Cast<UPrimitiveComponent>(GetRootComponent());
		float force = 0.0f;
		float torque = 0.0f;
		RotateKit(aimingInput);
		if (isGamepadInput)
		{
			force = movementInput.Size();
			if (force > 0)
			{
				if (backMovement)
					force = -force;

				movementInput.Normalize();

				FQuat qbetween = FQuat::FindBetween(GetActorForwardVector(), movementInput);
				FRotator rbetween(qbetween);
				float angle = FRotator::NormalizeAxis(rbetween.Yaw);
				if (angle > MinGamepadRotation || angle < -MinGamepadRotation)
					torque = FMath::Clamp(angle / MaxGamepadRotation, -1.0f, 1.0f);
			}
			else if (backMovement)
				force = -0.75f;
		}
		else
		{
			force = movementInput.X;
			if (movementInput.X < 0.0f)//����������� ����������, ����� ���� �����
			{
				torque = -movementInput.Y;
			}
			else
			{
				torque = movementInput.Y;
			}
		}

		if (force != 0.0f)
			root->AddForce(GetActorForwardVector() * force * MovementScale, NAME_None, true);
		if (torque != 0.0f)
		{
			float torquScale(1.0f);
			if (LinearVelocity < 400.0f)
				torquScale = UKismetMathLibrary::MapRangeClamped(LinearVelocity, 20.0f, 370.0f, 0.2f, 1.0f);
			else
				torquScale = UKismetMathLibrary::MapRangeClamped(LinearVelocity, 500.0f, 800.0f, 1.0f, 0.6f);
			
			root->AddTorque(FVector(0.0f, 0.0f, FMath::Clamp(torque, -0.75f, 1.0f) * RotationScale * torquScale), NAME_None, true);
			if (Role == ROLE_Authority)
				EngineAngle = FMath::Clamp(EngineAngle + torque * 100.0f * GetWorld()->GetDeltaSeconds(), -50.0f, 50.0f);
		}
		else
		{
			if (Role == ROLE_Authority)
				EngineAngle = FMath::FInterpTo(EngineAngle, 0.0f, GetWorld()->GetDeltaSeconds(), 0.75f);
		}

		//���� �� �� �������, ���� ����� �������, ����� ���� ��� ��������
		if (Role < ROLE_Authority)
		{
			ServerApplyInput(movementInput, aimingInput, backMovement, isGamepadInput);
		}
	}
}

void ABaseBoat::OnRep_ServerPhysicsState()
{
	// If we get here, we are always the client. Here we store the physics state
	// for physics state interpolation.

	// Shift the buffer sideways, deleting state PROXY_STATE_ARRAY_SIZE
	for (int i = PROXY_STATE_ARRAY_SIZE - 1; i >= 1; i--)
	{
		proxyStates[i] = proxyStates[i - 1];
	}

	// Record current state in slot 0
	proxyStates[0] = ServerPhysicsState;

	// Update used slot count, however never exceed the buffer size
	// Slots aren't actually freed so this just makes sure the buffer is
	// filled up and that uninitalized slots aren't used.
	proxyStateCount = FMath::Min(proxyStateCount + 1, PROXY_STATE_ARRAY_SIZE);

	// Check if states are in order
	if (proxyStates[0].timestamp < proxyStates[1].timestamp)
	{
		//UE_LOG(LogOnlineGame, Verbose, TEXT("Timestamp inconsistent: %d should be greater than %d"), proxyStates[0].timestamp, proxyStates[1].timestamp);
	}
}

/** Simulates the free movement of the ball based on proxy states */
void ABaseBoat::ClientSimulateMovingBoat()
{
	ABaseBoatPlayerController* boatPlayerController = Cast<ABaseBoatPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (nullptr == boatPlayerController || !boatPlayerController->IsNetworkTimeValid() || 0 == proxyStateCount)
	{
		// We don't know yet know what the time is on the server yet so the timestamps
		// of the proxy states mean nothing; that or we simply don't have any proxy
		// states yet. Don't do any interpolation.
		//SetActorLocationAndRotation(ServerPhysicsState.pos, ServerPhysicsState.rot);
	}
	else
	{
		uint64 interpolationBackTime = 100;
		uint64 extrapolationLimit = 500;

		// This is the target playback time of the rigid body
		uint64 interpolationTime = boatPlayerController->GetNetworkTime() - interpolationBackTime;

		// Use interpolation if the target playback time is present in the buffer
		if (proxyStates[0].timestamp > interpolationTime)
		{
			// Go through buffer-- and find correct state to play back
			for (int i = 0; i<proxyStateCount; i++)
			{
				if (proxyStates[i].timestamp <= interpolationTime || i == proxyStateCount - 1)
				{
					// The state one slot newer (<100ms) than the best playback state
					FSmoothBoatState rhs = proxyStates[FMath::Max(i - 1, 0)];
					// The best playback state (closest to 100 ms old (default time))
					FSmoothBoatState lhs = proxyStates[i];

					// Use the time between the two slots to determine if interpolation is necessary
					int64 length = (int64)(rhs.timestamp - lhs.timestamp);
					double t = 0.0F;
					// As the time difference gets closer to 100 ms t gets closer to 1 in
					// which case rhs is only used
					if (length > 1)
						t = (double)(interpolationTime - lhs.timestamp) / (double)length;

					// if t=0 => lhs is used directly					
					FVector pos = FMath::Lerp(lhs.pos, rhs.pos, t);
					FRotator rot = FMath::Lerp(lhs.rot, rhs.rot, t);
					FVector vel = FMath::Lerp(lhs.vel, rhs.vel, t);
					LinearVelocity = vel.Size();
					EngineAngle = FMath::Lerp(lhs.engineAngle, rhs.engineAngle, t);
					if(!IsLocallyControlled())
						KitAngle = FMath::Lerp(lhs.kitAngle, rhs.kitAngle, t);
					//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("SetActorLocationAndRotation x: %f, y: %f"), pos.X, pos.Y));
					SetActorLocationAndRotation(pos, rot);
					return;
				}
			}
		}
		// Use extrapolation
		else
		{
			FSmoothBoatState latest = proxyStates[0];

			uint64 extrapolationLength = interpolationTime - latest.timestamp;
			// Don't extrapolate for more than [extrapolationLimit] milliseconds
			if (extrapolationLength < extrapolationLimit)
			{
				LinearVelocity = latest.vel.Size();
				EngineAngle = latest.engineAngle;
				if (!IsLocallyControlled())
					KitAngle = latest.kitAngle;
				FVector pos = latest.pos + latest.vel * ((float)extrapolationLength * 0.001f);
				FRotator rot = FRotator(latest.rot.Roll, latest.rot.Yaw + latest.torque * ((float)extrapolationLength * 0.001f), latest.rot.Pitch);
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("SetActorLocationAndRotation x: %f, y: %f"), pos.X, pos.Y));
				SetActorLocationAndRotation(pos, rot);
			}
			else
			{
				// Don't move. If we're this far away from the server, we must be pretty laggy.
				// Wait to catch up with the server.
			}
		}
	}
}

// Sets default values
ABaseBoat::ABaseBoat()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementScale = 100.0f;
	RotationScale = 17.0f;
	EngineAngle = 0.0f;
	KitAngle = 0.0f;
}

// Called when the game starts or when spawned
void ABaseBoat::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABaseBoat::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// No possessor. The ball is freely moving.
	if (Role < ROLE_Authority)// && !IsLocallyControlled())
	{
		// Clients should update its local position based on where it is on the server		
		ClientSimulateMovingBoat();
	}
	else
	{
		// Servers should simulate the ball physics freely and replicate the orientation
		UPrimitiveComponent *Root = Cast<UPrimitiveComponent>(GetRootComponent());
		ServerPhysicsState.pos = GetActorLocation();
		ServerPhysicsState.rot = GetActorRotation();
		ServerPhysicsState.vel = Root->GetComponentVelocity();
		ServerPhysicsState.torque = Root->GetPhysicsAngularVelocity().Z;
		ServerPhysicsState.timestamp = ABaseBoatPlayerController::GetLocalTime();
		ServerPhysicsState.engineAngle = EngineAngle;
		ServerPhysicsState.kitAngle = KitAngle;
		LinearVelocity = ServerPhysicsState.vel.Size();
	}
}

// Called to bind functionality to input
void ABaseBoat::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}
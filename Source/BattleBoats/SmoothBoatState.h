// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SmoothBoatState.generated.h"
/**
 * 
 */
USTRUCT()
struct BATTLEBOATS_API FSmoothBoatState
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	uint64 timestamp;
	UPROPERTY()
	FVector pos;
	UPROPERTY()
	FVector vel;
	UPROPERTY()
	FRotator rot;
	UPROPERTY()
	float torque;
	UPROPERTY()
	float engineAngle;
	UPROPERTY()
	float kitAngle;

	FSmoothBoatState()
	{
		timestamp = 0;
		torque = 0.0f;
		pos = FVector::ZeroVector;
		vel = FVector::ZeroVector;
		rot = FRotator::ZeroRotator;
	}
};

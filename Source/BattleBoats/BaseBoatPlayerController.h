// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "BaseBoatPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLEBOATS_API ABaseBoatPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ABaseBoatPlayerController(const FObjectInitializer& ObjectInitializer);

	/** True if the network time is valid. */
	bool IsNetworkTimeValid();

	/** Gets the current system time in milliseconds */
	static int64 GetLocalTime();

	/** Gets the approximate current network time in milliseconds. */
	int64 GetNetworkTime();

	/** This occurs when play begins */
	virtual void BeginPlay() override;

protected:
	int64 timeServerTimeRequestWasPlaced;
	int64 timeOffsetFromServer;
	bool timeOffsetIsValid;

	/** Sent from a client to the server to get the server's system time */
	UFUNCTION(reliable, server, WithValidation)
		void ServerGetServerTime();

	/** Sent from the server to a client to give them the server's system time */
	UFUNCTION(reliable, client)
		void ClientGetServerTime(int64 serverTime);
	
	
};

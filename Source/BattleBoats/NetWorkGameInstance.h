// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "AdvancedFriendsGameInstance.h"
#include "NetWorkGameInstance.generated.h"


UENUM(BlueprintType)		//���������� ������
enum class EResolutionEnum : uint8
{
	VE_Res800x600 		UMETA(DisplayName = "Res800x600"),
	VE_Res1024x768 		UMETA(DisplayName = "Res1024x768"),
	VE_Res1280x720 		UMETA(DisplayName = "Res1280x720"),
	VE_Res1280x1024 	UMETA(DisplayName = "Res1280x1024"),
	VE_Res1600x900 		UMETA(DisplayName = "Res1600x900"),
	VE_Res1920x1080		UMETA(DisplayName = "Res1920x1080")
};

UENUM(BlueprintType)		//�������� �����������
enum class EQualityEnum : uint8
{
	VE_Low 		UMETA(DisplayName = "Low"),
	VE_Medium 	UMETA(DisplayName = "Medium"),
	VE_High		UMETA(DisplayName = "High"),
	VE_Epic		UMETA(DisplayName = "Epic")
};

UENUM(BlueprintType)		//�����������
enum class ELocalizationEnum : uint8
{
	VE_En		UMETA(DisplayName = "en"),
	VE_Ru 	UMETA(DisplayName = "ru")
};

UENUM(BlueprintType)		//����� ����
enum class EDisplayModeEnum : uint8
{
	VE_Fullscreen 	UMETA(DisplayName = "Fullscreen"),
	VE_Window 		UMETA(DisplayName = "Window")
};

UCLASS()
class BATTLEBOATS_API UNetWorkGameInstance : public UAdvancedFriendsGameInstance
{
	GENERATED_BODY()
public:

	// Constuctor

	UNetWorkGameInstance(const FObjectInitializer& ObjectInitializer);

private:
	UGameUserSettings* settings;

public:
	//Functions

	UFUNCTION(BlueprintCallable, Category = "Settings")
	EResolutionEnum GetResolution();

	UFUNCTION(BlueprintCallable, Category = "Settings")
	EQualityEnum GetQuality();

	UFUNCTION(BlueprintCallable, Category = "Settings")
	EDisplayModeEnum GetDisplayMode();

	UFUNCTION(BlueprintCallable, Category = "Settings")
	bool SaveSettings(EResolutionEnum resolution, EQualityEnum quality, EDisplayModeEnum displayMode);
};

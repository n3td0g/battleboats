// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "SmoothBoatState.h"
#include "BaseBoat.generated.h"

#define PROXY_STATE_ARRAY_SIZE 20

UCLASS()
class BATTLEBOATS_API ABaseBoat : public APawn
{
	GENERATED_BODY()

	/** The soccer ball orientation on the server */
	UPROPERTY(ReplicatedUsing = OnRep_ServerPhysicsState)
	FSmoothBoatState ServerPhysicsState;

	UFUNCTION()
	void OnRep_ServerPhysicsState();
private:
	/** Clients store twenty states with "playback" information from the server. This
	array contains the official state of this object at different times according to
	the server. */
	FSmoothBoatState proxyStates[PROXY_STATE_ARRAY_SIZE];

	/** Keep track of what slots are used */
	int proxyStateCount;

	/** Simulates the free movement of the ball based on proxy states */
	void ClientSimulateMovingBoat();

	void RotateKit(FVector direction);
public:
	UPROPERTY(BlueprintReadWrite, Category = Movement)
	float LinearVelocity;

	UPROPERTY(BlueprintReadWrite, Category = Movement)
	float KitAngle;

	UPROPERTY(BlueprintReadWrite, Category = Movement)
	float EngineAngle;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = Movement)
	float MovementScale;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = Movement)
	float RotationScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float MaxGamepadRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float MinGamepadRotation;

	/** Sent from a client to the server to get the server's system time */
	UFUNCTION(reliable, server, WithValidation)
	void ServerApplyInput(FVector movementInput, FVector aimingInput, bool backMovement, bool isGamepadInput);

	UFUNCTION(BlueprintCallable, Category = Movement)
	void ApplyInput(FVector movementInput, FVector aimingInput, bool backMovement, bool isGamepadInput);

	// Sets default values for this pawn's properties
	ABaseBoat();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
};

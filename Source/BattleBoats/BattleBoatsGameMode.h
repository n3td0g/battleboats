// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "BattleBoatsGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BATTLEBOATS_API ABattleBoatsGameMode : public AGameMode
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, Category = "Network|Test")
	void KickPlayer(APlayerController * KickedPlayer, FText KickReason);	
};

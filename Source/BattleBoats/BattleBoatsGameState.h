// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "BattleBoatsGameState.generated.h"

/**
 * 
 */
UCLASS()
class BATTLEBOATS_API ABattleBoatsGameState : public AGameState
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "RoundInfo")
	int32 NumOfRevivals;
		
};

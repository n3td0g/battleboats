// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "BattleBoatsPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class BATTLEBOATS_API ABattleBoatsPlayerState : public APlayerState
{
	GENERATED_BODY()	
	
public:
	UPROPERTY(BlueprintReadWrite, Replicated, Category = "PlayerInfo")
	bool IsReady;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "PlayerInfo")
	int32 Team;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "PlayerInfo")
	FLinearColor TeamColor;

	virtual void CopyProperties(class APlayerState* PlayerState) override;
	virtual void SeamlessTravelTo(class APlayerState* NewPlayerState) override;
};
